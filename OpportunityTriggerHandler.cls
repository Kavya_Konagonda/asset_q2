public class OpportunityTriggerHandler {
public static void postChatter(List<Opportunity> newList, Map<Id,Opportunity> oldMap){
        List<FeedItem> feedItemToInsert = new List<FeedItem>();
        for(Opportunity opp : newList){
            if(((oldMap == null) || (oldMap != null && opp.Assigned_To__c != oldMap.get(opp.Id).Assigned_To__c ))&& opp.Assigned_To__c != null && opp.Amount>10000){
                ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
                messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            
                ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
                textSegment.text = 'Hi ';
                messageInput.messageSegments.add(textSegment);
            
                ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
                mentionSegment.id = opp.Assigned_To__c; // The ID of the user to mention.
                messageInput.messageSegments.add(mentionSegment);
            
                textSegment = new ConnectApi.TextSegmentInput();
                textSegment.text = ' \nPlease check this Opportunity ';
                messageInput.messageSegments.add(textSegment);
            
                ConnectApi.EntityLinkSegmentInput entityLinkSegment = new ConnectApi.EntityLinkSegmentInput();
                entityLinkSegment.entityId = opp.Id;
                messageInput.messageSegments.add(entityLinkSegment);
                
                textSegment = new ConnectApi.TextSegmentInput();
                textSegment.text = ' \n\nThank you ';
                messageInput.messageSegments.add(textSegment);
            
                ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
                input.body = messageInput;
                input.subjectId = opp.Id;//record Id
                ConnectApi.FeedItem fi =  (ConnectApi.FeedItem) ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), input);
            }
        }
    }
}